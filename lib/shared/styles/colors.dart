import 'package:flutter/widgets.dart';

abstract class AppColors {
  static const Color body = Color(0xFFF2F2F2);
  static const Color primary = Color.fromARGB(255, 93, 54, 116);
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color boxShadowColor = Color(0xFFF0F0F0);
  static const Color etColor = Color(0xFF736F6E);
  static const Color kTextColor = Color(0xFF535353);
}
