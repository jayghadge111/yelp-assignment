import 'package:assignmnet/shared/widgets/flutter_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';

class LoginScreenController extends GetxController {
  late PageController pageController;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  void onInit() {
    pageController = PageController()
      ..addListener(() {
        update();
      });

    super.onInit();
  }

  navigateToBusinessList() async {
    if (username.text.isEmpty || username.text != 'admin') {
      showToast(message: 'Please enter valid username');
    } else if (password.text.isEmpty || password.text != 'admin') {
      showToast(message: 'Please enter valid password');
    } else {
      Get.toNamed(AppRoutes().businessList);
    }
  }
}
