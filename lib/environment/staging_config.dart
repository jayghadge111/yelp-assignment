import 'environment.dart';
import 'base_config.dart';

class StagingConfig implements BaseConfig {
  @override
  String get apiClient => 'MOBILE';

  @override
  String get env => Environment.STAGING;

  @override
  String get baseurl => 'https://api.yelp.com/v3';

  @override
  String get defaultToken =>
      'Bearer bza6Hp9uiEWv2F86OSEycfB9Sc7-7G9FfMZ1JgtQM3cqSpEnG6CsF_IzpEmouNAdfiXcvmL4DSbMqbsp6jmWpyKJ-DG7-FzN3hfqXxjvQ_jfgvZQWXr5yN9pdIrjZXYx';
}
