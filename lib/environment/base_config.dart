abstract class BaseConfig {
  String get env;
  String get baseurl;
  String get defaultToken;
  String get apiClient;
}
