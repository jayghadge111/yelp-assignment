// ignore_for_file: public_member_api_docs, sort_constructors_first, must_be_immutable
import 'package:flutter/material.dart';

import 'package:assignmnet/shared/styles/decoration.dart';

import '../../../../shared/styles/app_space_between.dart';
import '../../../../shared/styles/colors.dart';
import '../../../../shared/widgets/star_rating.dart';
import '../../models/categories_model.dart';

class SingleBusinessCard extends StatelessWidget {
  final String imageUrl;
  final String name;
  final double rating;
  final int reviewCount;
  final String displayAddress;
  List<Categories>? businessCategory;
  String? phone;
  List<String>? transaction;
  bool? isClosed;

  SingleBusinessCard({
    Key? key,
    required this.imageUrl,
    required this.name,
    required this.rating,
    required this.reviewCount,
    required this.displayAddress,
    this.businessCategory,
    this.phone,
    this.transaction,
    this.isClosed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        decoration: AppDecoration.containerBoxDecoration(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.20,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius:
                    const BorderRadius.vertical(top: Radius.circular(15)),
                gradient: LinearGradient(
                  colors: [Colors.purple.shade800, Colors.purple.shade500],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  tileMode: TileMode.clamp,
                ),
                image: DecorationImage(
                  image: NetworkImage(imageUrl),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text((isClosed ?? false) ? '(Temporary Closed)' : '(Open)'),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          StarRating(
                            onRatingChanged: (rating) {},
                            starCount: 5,
                            rating: rating,
                            color: Colors.yellow,
                          ),
                          gapW4,
                          Text('(${rating.toString()})'),
                        ],
                      ),
                      Text('$reviewCount reviews'),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Text('Address: $displayAddress'),
                  Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('Contact Number: ${phone ?? ''}')),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
