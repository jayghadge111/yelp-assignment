import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'colors.dart';

abstract class AppDecoration {
  static BoxDecoration containerBoxDecoration(
      {double radius = 15,
      Color color = AppColors.white,
      double blurRadius = 3.0}) {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(radius),
      color: color,
      boxShadow: [
        BoxShadow(
          color: AppColors.boxShadowColor,
          blurRadius: blurRadius,
          spreadRadius: 3.0,
        )
      ],
    );
  }

  static PinTheme pinFieldDecoration() {
    return PinTheme(
      shape: PinCodeFieldShape.box,
      borderRadius: BorderRadius.circular(5),
      fieldHeight: 45,
      fieldWidth: 45,
      activeFillColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      inactiveFillColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      selectedFillColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      activeColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      inactiveColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      selectedColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      errorBorderColor: const Color(0xFFF5F5F5).withOpacity(0.3686),
      borderWidth: 0,
      inactiveBorderWidth: 0,
      activeBorderWidth: 0,
      selectedBorderWidth: 0,
      errorBorderWidth: 0,
    );
  }
}
