import 'package:assignmnet/shared/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../../../shared/styles/app_space_between.dart';
import '../controller/business_controller.dart';
import 'component/business_card.dart';

class BusinessList extends GetView<BusinessController> {
  const BusinessList({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.body,
      appBar: AppBar(
        leadingWidth: 0,
        leading: const SizedBox(),
        title: const Text(
          'Business List',
          style: TextStyle(fontSize: 20),
        ),
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Colors.purple.shade800, Colors.purple.shade500],
            ),
          ),
        ),
      ),
      body: Obx(
        () => controller.dataLoading.value
            ? const Center(child: CircularProgressIndicator())
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ListView.separated(
                  separatorBuilder: (context, index) => gapH16,
                  itemCount: controller.businessList.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () async {
                        Get.toNamed(AppRoutes().businessDetails, arguments: {
                          'singleBusiness': controller.businessList[index],
                        });
                      },
                      child: SingleBusinessCard(
                        imageUrl: controller.businessList[index].imageUrl!,
                        name: controller.businessList[index].name ?? '',
                        rating: controller.businessList[index].rating ?? 0.0,
                        reviewCount:
                            controller.businessList[index].reviewCount ?? 0,
                        displayAddress: controller
                                .businessList[index].location?.displayAddress
                                ?.join(', ') ??
                            '',
                      ),
                    );
                  },
                ),
              ),
      ),
    );
  }
}
