import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showToast({
  String? message,
  Color? backgroundcolor,
  Color? textColor,
}) {
  Fluttertoast.showToast(
    msg: message ?? '',
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 10,
    backgroundColor: backgroundcolor ?? Colors.black,
    textColor: textColor ?? Colors.white,
    fontSize: 14.0,
  );
}
