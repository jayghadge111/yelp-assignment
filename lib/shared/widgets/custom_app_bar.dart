import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../styles/colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool showLeading;

  const CustomAppBar({Key? key, required this.title, this.showLeading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leadingWidth: 0,
      leading: InkWell(
        onTap: () {
          Get.back();
        },
        child: showLeading
            ? const Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Icon(Icons.arrow_back_ios, color: AppColors.white),
              )
            : const SizedBox(),
      ),
      title: Text(
        title,
        style: const TextStyle(fontSize: 20),
      ),
      backgroundColor: Colors.transparent,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Colors.purple.shade800, Colors.purple.shade500],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

// Usage:


