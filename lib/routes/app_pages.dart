import 'package:get/get.dart';

import '../bindings/auth_binding.dart';
import '../bindings/business_binding.dart';
import '../features/authentication/view/login.dart';
import '../features/business/view/business_details.dart';
import '../features/business/view/business_list.dart';
import 'import.dart';
part 'app_routes.dart';

class AppPages {
  AppPages._internal();
  static final AppPages _instance = AppPages._internal();

  factory AppPages() {
    return _instance;
  }

  final routes = [
    GetPage(
      name: AppRoutes().splashScreen,
      page: () => const SplashScreen(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: AppRoutes().login,
      page: () => const Login(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoutes().businessList,
      page: () => const BusinessList(),
      binding: BusinessListBinding(),
    ),
    GetPage(
      name: AppRoutes().businessDetails,
      page: () => const BusinessDetails(),
      binding: BusinessDetailsBinding(),
    ),
  ];
}
