import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../shared/styles/app_space_between.dart';
import '../../../shared/styles/colors.dart';
import '../../../shared/widgets/text_input_field.dart';
import '../controller/login_controller.dart';

class Login extends GetView<LoginScreenController> {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Login'),
      // ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Center(
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                    ),
                  ),
                ),
                gapH24,
                Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'User Name',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 15,
                          color: AppColors.kTextColor,
                        ),
                      ),
                      gapH4,
                      TextInputField(
                        type: TextInputType.number,
                        hint: 'Enter your username',
                        etIcon: 'assets/icons/smartphone.png',
                        controller: controller.username,
                        isProtect: false,
                        maxLength: 10,
                        isMobile: true,
                        // error: mobValidate ? mobValidationMsg : null,
                      ),
                      gapH20,
                      const Text(
                        'Password',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          color: AppColors.kTextColor,
                        ),
                      ),
                      gapH4,
                      TextInputField(
                        type: TextInputType.text,
                        hint: 'Enter your password',
                        maxLength: 20,
                        isProtect: true,
                        isPass: true,
                        // etIcon: "assets/icons/smartphone.png",
                        controller: controller.password,
                      ),
                    ],
                  ),
                ),
                gapH24,
                GestureDetector(
                  onTap: () {
                    controller.navigateToBusinessList();
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24, vertical: 15),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        stops: [0.4, 0.8],
                        colors: [
                          AppColors.primary,
                          Color.fromARGB(255, 139, 33, 146)
                        ],
                      ),
                    ),
                    child: const DefaultTextStyle(
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(
                            fit: StackFit.passthrough,
                            children: [
                              FadeTransition(
                                opacity: AlwaysStoppedAnimation(1),
                                child: Text(
                                  'Sign In',
                                  maxLines: 1,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
