class FlutterStorageKeys {
  FlutterStorageKeys._internal();

  static final FlutterStorageKeys _instance = FlutterStorageKeys._internal();

  factory FlutterStorageKeys() {
    return _instance;
  }

  final String loginDetails = 'loginDetails';
  final String isBiomatricEnabled = 'isBiomatricEnabled';
  final String loginPin = 'loginPin';
  final String token = 'token';
}
