part of 'app_pages.dart';

class AppRoutes {
  AppRoutes._internal();

  static final AppRoutes _instance = AppRoutes._internal();

  factory AppRoutes() {
    return _instance;
  }

  final String splashScreen = '/';
  final String login = '/login';
  final String businessList = '/businessList';
  final String businessDetails = '/businessDetails';
}
