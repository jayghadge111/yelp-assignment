import 'dart:async';
import 'dart:developer';

import 'package:flutter/widgets.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sizer/sizer.dart';

import 'environment/environment.dart';
import 'init_app.dart';

void main() async {
  bootstrap(() {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return const InitApp();
      },
    );
  });
}

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };

  WidgetsFlutterBinding.ensureInitialized();

  const String environment = String.fromEnvironment(
    'ENVIRONMENT',
    defaultValue: Environment.STAGING,
  );

  Environment().initConfig(environment);
  await GetStorage.init();

  await runZonedGuarded(
    () async => runApp(await builder()),
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}
