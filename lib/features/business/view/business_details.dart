import 'package:assignmnet/shared/styles/colors.dart';
import 'package:assignmnet/shared/styles/decoration.dart';
import 'package:assignmnet/shared/widgets/star_rating.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../shared/widgets/custom_app_bar.dart';
import '../controller/business_details_controller.dart';

class BusinessDetails extends GetView<BusinessDetailsController> {
  const BusinessDetails({super.key});

  @override
  Widget build(BuildContext context) {
    double cardHeight = MediaQuery.of(context).size.height * 0.4;

    return Scaffold(
      appBar: const CustomAppBar(
        title: 'Business Details',
        // showLeading: true,
      ),
      body: Obx(() => controller.singleBusinessDetails?.id == null
          ? const Center(child: CircularProgressIndicator())
          : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0),
              child: Container(
                decoration: AppDecoration.containerBoxDecoration(),
                child: Column(
                  children: [
                    Container(
                      height: cardHeight,
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: const BorderRadius.vertical(
                            top: Radius.circular(0)),
                        gradient: LinearGradient(
                          colors: [
                            Colors.purple.shade800,
                            Colors.purple.shade500
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          tileMode: TileMode.clamp,
                        ),
                        image: DecorationImage(
                          image: NetworkImage(
                              controller.singleBusinessDetails?.imageUrl ?? ''),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.singleBusinessDetails?.name ?? '',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text((controller.singleBusinessDetails?.isClosed ??
                                  false)
                              ? '(Temporary Closed)'
                              : '(Open)'),
                          const SizedBox(height: 8),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  StarRating(
                                    onRatingChanged: (rating) {},
                                    starCount: 5,
                                    rating: controller
                                            .singleBusinessDetails?.rating ??
                                        0,
                                    color: Colors.yellow,
                                  ),
                                  const Icon(Icons.star, color: Colors.yellow),
                                  Text(
                                      '(${controller.singleBusinessDetails?.rating.toString()})'),
                                ],
                              ),
                              Text(
                                  '${controller.singleBusinessDetails?.reviewCount} reviews'),
                            ],
                          ),
                          const SizedBox(height: 8),
                          Text('Address: ${controller.address}'),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text('Category: ${controller.categories}'),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                  'Phone Number: ${controller.singleBusinessDetails?.phone ?? ''}')),
                          Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                  'Services Provided: ${controller.serviceTypeProvided}')),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )),
    );
  }
}
