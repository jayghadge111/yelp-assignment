import 'package:assignmnet/core/api_service.dart';
import 'package:assignmnet/features/business/models/single_business_model.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class BusinessController extends GetxController {
  final ApiService _apiService = ApiService();

  RxList<Businesses> businessList = <Businesses>[].obs;

  RxBool dataLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    fetchBusinesses();
  }

  Future<void> fetchBusinesses() async {
    try {
      businessList.value = await _apiService.getBusinesses();
      dataLoading(false);
      if (kDebugMode) {
        print('Received businesses: $businessList.value');
      }
    } catch (error) {
      rethrow;
    }
  }
}
