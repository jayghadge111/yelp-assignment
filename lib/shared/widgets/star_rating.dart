import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// ignore: prefer_generic_function_type_aliases
typedef void RatingChangeCallback(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback onRatingChanged;
  final Color? color;

  const StarRating(
      {super.key,
      this.starCount = 5,
      this.rating = .0,
      required this.onRatingChanged,
      this.color});

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = Icon(
        Icons.star_border,
        color: Colors.grey.shade500,
        size: 5.h,
      );
    }
    // else if (index > rating - 1 && index < rating) {
    //   icon = Icon(
    //     Icons.star_half,
    //     color: color ?? AramcoColors.yellow,
    //   );
    // }
    else {
      icon = Icon(
        Icons.star,
        color: color ?? Colors.yellow,
        size: 2.75.h,
      );
    }
    return InkResponse(
      onTap:
          // ignore: unnecessary_null_comparison
          onRatingChanged == null ? null : () => onRatingChanged(index + 1.0),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:
            List.generate(starCount, (index) => buildStar(context, index)));
  }
}
