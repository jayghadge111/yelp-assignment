import 'package:flutter/material.dart';

import '../styles/colors.dart';
import '../styles/decoration.dart';

class TextInputField extends StatefulWidget {
  final Function()? onTap;
  final TextInputType type;
  final String? etIcon;
  final String hint;
  final String? error;
  final int maxLength;
  final TextEditingController controller;
  final bool isProtect;
  final bool? isMobile;
  final bool? isEmail;
  final bool? isPass;
  final bool? isOtp;
  final bool? isKit;
  final bool? isCard;
  final bool? isName;
  final bool? isAmount;
  final bool? readOnly;
  final Widget? prefixIcon;
  final String? Function(String?)? validator;

  const TextInputField({
    super.key,
    this.onTap,
    required this.isProtect,
    required this.maxLength,
    required this.type,
    this.etIcon,
    required this.hint,
    this.error,
    required this.controller,
    this.isMobile,
    this.isEmail,
    this.isPass,
    this.isOtp,
    this.isKit,
    this.isCard,
    this.isName,
    this.isAmount,
    this.readOnly,
    this.prefixIcon,
    this.validator,
  });

  @override
  State<TextInputField> createState() => _TextInputFieldState();
}

class _TextInputFieldState extends State<TextInputField> {
  bool isPassVisible = false;

  void _toggle() {
    setState(() {
      isPassVisible = !isPassVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: AppDecoration.containerBoxDecoration(),
      child: TextFormField(
        validator: widget.validator,
        onTap: widget.onTap,
        keyboardType: widget.type,
        style: const TextStyle(color: AppColors.kTextColor),
        controller: widget.controller,
        maxLength: widget.maxLength,
        maxLines: 1,
        obscureText: widget.isProtect ? !isPassVisible : false,
        enabled: true,
        readOnly: widget.readOnly == true ? true : false,
        decoration: InputDecoration(
            counterText: '',
            filled: true,
            fillColor: Colors.white,
            contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border: InputBorder.none,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: const BorderSide(color: Colors.white, width: 3.0),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(width: 1, color: Colors.orange),
            ),
            hintText: widget.hint,
            hintStyle: TextStyle(fontSize: 14, color: Colors.grey[500]),
            suffixIcon: widget.isProtect
                ? IconButton(
                    icon: isPassVisible
                        ? const Icon(Icons.visibility, color: Colors.grey)
                        : const Icon(Icons.visibility_off, color: Colors.grey),
                    onPressed: _toggle,
                  )
                : const SizedBox(height: 0, width: 0),
            prefixIcon: widget.prefixIcon

            // widget.isMobile != null
            //     ? IconButton(
            //         icon: const Icon(Icons.call, color: odinmoColor),
            //         onPressed: () {},
            //       )
            //     : widget.isPass != null
            //         ? IconButton(
            //             icon: const Icon(Icons.lock, color: odinmoColor),
            //             onPressed: () {},
            //           )
            //         : widget.isOtp != null
            //             ? IconButton(
            //                 icon: const Icon(Icons.phone_android,
            //                     color: odinmoColor),
            //                 onPressed: () {},
            //               )
            //             : widget.isEmail != null
            //                 ? IconButton(
            //                     icon: const Icon(Icons.email, color: odinmoColor),
            //                     onPressed: () {},
            //                   )
            //                 : widget.isKit != null
            //                     ? IconButton(
            //                         icon: const Icon(
            //                             Icons.confirmation_number_outlined,
            //                             color: odinmoColor),
            //                         onPressed: () {},
            //                       )
            //                     : widget.isCard != null
            //                         ? IconButton(
            //                             icon: const Icon(Icons.credit_card,
            //                                 color: odinmoColor),
            //                             onPressed: () {},
            //                           )
            //                         : widget.isName != null
            //                             ? IconButton(
            //                                 icon: const Icon(Icons.person,
            //                                     color: odinmoColor),
            //                                 onPressed: () {},
            //                               )
            //                             : widget.isAmount != null
            //                                 ? IconButton(
            //                                     icon: const Icon(
            //                                         Icons.app_registration,
            //                                         color: odinmoColor),
            //                                     onPressed: () {},
            //                                   )
            //                                 : const SizedBox(height: 0, width: 0),
            // errorText: widget.error ?? null,
            ),
      ),
    );
  }
}
