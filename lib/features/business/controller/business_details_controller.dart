import 'package:get/get.dart';

import '../models/categories_model.dart';
import '../models/single_business_model.dart';

class BusinessDetailsController extends GetxController {
  final Rx<Businesses?> _singleBusinessDetails = Businesses().obs;
  Businesses? get singleBusinessDetails => _singleBusinessDetails.value;
  Map<String, dynamic>? data = {};
  RxString address = ''.obs;
  RxString serviceTypeProvided = ''.obs;
  RxString categories = ''.obs;

  @override
  void onInit() {
    super.onInit();
    getArguments();
  }

  getArguments() async {
    List<String> categoryList = [];
    data = await Get.arguments;
    if (data != null) {
      _singleBusinessDetails.value = data!['singleBusiness'];
      address.value =
          '${_singleBusinessDetails.value!.location!.address1 ?? ''} ${_singleBusinessDetails.value!.location!.address2 ?? ''} ${_singleBusinessDetails.value!.location!.address3 ?? ''} ${_singleBusinessDetails.value!.location!.city ?? ''} ${_singleBusinessDetails.value!.location!.state ?? ''} ${_singleBusinessDetails.value!.location!.zipCode ?? ''} ${_singleBusinessDetails.value!.location!.country ?? ''}';
      List<String> serviceList = _singleBusinessDetails.value!.transactions!;
      serviceTypeProvided.value = serviceList.join(', ');
      for (Categories data in _singleBusinessDetails.value!.categories!) {
        String title = data.title!;
        categoryList.add(title);
      }
      categories.value = categoryList.join(', ');
    }
    update();
  }
}
