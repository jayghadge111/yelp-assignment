import 'package:assignmnet/core/api_end_points.dart';
import 'package:assignmnet/environment/environment.dart';

import '../features/business/models/single_business_model.dart';
import 'http_provider.dart';

class ApiService {
  final HttpsProvider httpsProvider = HttpsProvider(
    baseUrl: Environment().config?.baseurl ?? '',
  );

  Future<List<Businesses>> getBusinesses() async {
    try {
      final Map<String, dynamic> response = await httpsProvider.get(
        '${ApiEndPoints().businessBaseOnLocation}${"NYC"}',
        headers: {
          'Authorization': Environment().config?.defaultToken ?? '',
        },
      );
      final List<dynamic> businesses = response['businesses'];
      print(businesses);
      return businesses.map((business) {
        return Businesses.fromJson(business);
      }).toList();
    } catch (e) {
      throw Exception('Failed to load business: $e');
    }
  }
}
