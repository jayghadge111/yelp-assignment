class ApiEndPoints {
  ApiEndPoints._internal();
  static final ApiEndPoints _instance = ApiEndPoints._internal();
  factory ApiEndPoints() {
    return _instance;
  }

  /// API END POINTS
  final String businessBaseOnLocation = 'businesses/search?location=';
}
