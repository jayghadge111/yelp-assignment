import 'package:get/get.dart';

import '../features/business/controller/business_controller.dart';
import '../features/business/controller/business_details_controller.dart';

class BusinessListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BusinessController>(() => BusinessController());
  }
}

class BusinessDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BusinessDetailsController>(() => BusinessDetailsController());
  }
}
